package com.github.chaabaj.githubstats

import akka.http.scaladsl.Http
import akka.util.Timeout
import com.github.chaabaj.githubstats.core.Concurrency
import scala.concurrent.Future
import scala.concurrent.duration._

object Server {
  implicit val timeout: Timeout = Timeout(10.seconds)
  def start(port: Int, infra: Infrastructure): Future[Http.ServerBinding] = {
    Concurrency.withActorSystem { implicit actorSystem =>
      Concurrency.withMaterializer { implicit materializer =>
        Http().bindAndHandle(new Routes(infra).routes, "0.0.0.0", port)
      }
    }
  }
}
