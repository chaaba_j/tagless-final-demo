package com.github.chaabaj.githubstats.core.datas

case class Credentials(url: String, token: String)
