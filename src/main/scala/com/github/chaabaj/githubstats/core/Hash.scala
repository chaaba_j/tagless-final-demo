package com.github.chaabaj.githubstats.core

import com.github.chaabaj.githubstats.core.Result.{AppError, HashFailed}
import com.github.chaabaj.githubstats.core.datas.Hashed
import org.mindrot.jbcrypt.BCrypt

import scala.util.Try
import scala.util.control.NonFatal

object Hash {

  val salt = BCrypt.gensalt(12)

  def bcrypt(str: String): Either[AppError, Hashed] = {
    val res = Try(
      Hashed(BCrypt.hashpw(str, salt))
    ).toEither

    res match {
      case Right(hash) => Right(hash)
      case Left(NonFatal(ex)) => Left(HashFailed(ex.getMessage))
      case Left(ex) =>
        // it's a fatal error please exit the jvm
        throw ex
    }
  }


  def check(str: String, hash: Hashed): Boolean =
    BCrypt.checkpw(str, hash.value)
}
