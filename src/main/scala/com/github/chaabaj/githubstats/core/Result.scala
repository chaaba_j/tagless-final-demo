package com.github.chaabaj.githubstats.core

import cats.data.EitherT
import com.github.chaabaj.githubstats.core.Result.{AppError, CombinedError}

import scala.reflect.ClassTag

object Result {
  sealed trait AppError extends RuntimeException
  case class UnknownError() extends AppError
  case class NotFound[A](reason: String)(implicit classTag: ClassTag[A]) extends AppError {
    override def getMessage: String =
      s"Cannot find entity of type $classTag with reason: $reason"
  }
  case class DeserializationError(cause: String) extends AppError {
    override def getMessage: String = cause
  }
  case class ServiceError[E](status: Int, content: Option[E]) extends AppError {
    override def getMessage: String =
      s"Request failed with status $status and content $content"
  }
  case class CombinedError(errors: Seq[AppError]) extends AppError {
    override def getMessage: String =
      errors.map(_.getMessage).mkString("\n")
  }
  case class HashFailed(reason: String) extends AppError {
    override def getMessage: String =
      reason
  }

  type Result[F[_], A] = F[Either[AppError, A]]
  val HandleError = EitherT
}

object AppError {
  def combine(errors: AppError*): AppError =
    CombinedError(errors)
}