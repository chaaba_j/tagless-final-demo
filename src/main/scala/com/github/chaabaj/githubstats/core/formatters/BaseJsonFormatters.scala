package com.github.chaabaj.githubstats.core.formatters

import com.github.chaabaj.githubstats.core.datas._
import spray.json.{DeserializationException, JsNumber, JsValue, RootJsonFormat}
import spray.json._
import DefaultJsonProtocol._

object BaseJsonFormatters {

  class IdFormat[A] extends RootJsonFormat[Id[A]] {
    override def read(json: JsValue): Id[A] = json match {
      case JsNumber(idVal) => Id(idVal.toInt)
      case _ =>
        throw DeserializationException(s"Expected a js number got ${json.prettyPrint}")
    }

    override def write(id: Id[A]): JsValue = JsNumber(id.value)
  }

  implicit val apiError = jsonFormat1(ApiError)
}
