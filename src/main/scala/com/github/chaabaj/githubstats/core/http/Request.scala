package com.github.chaabaj.githubstats.core.http

import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpRequest
import com.github.chaabaj.githubstats.core.{AppError, Concurrency, TaskLogger}
import com.github.chaabaj.githubstats.core.Result._
import monix.eval.Task
import spray.json.JsonFormat

import scala.concurrent.ExecutionContext
import scala.util.Try
import spray.json._

import scala.util.control.NonFatal

object Request {
  private val logger = new TaskLogger("http-request")
  private implicit val exc: ExecutionContext =
    Concurrency.dispatchers.global.scheduler
  private lazy val http =
    Concurrency.withActorSystem { implicit actorSystem =>
      Http()
    }

  private def parseJson[A](content: String,
                           format: JsonFormat[A]): Either[AppError, A] =
    Try(content.parseJson.convertTo(format))
      .map(Right.apply)
      .recover {
        case NonFatal(ex) =>
          Left(DeserializationError(ex.getMessage))
        case ex: Throwable =>
          // Exit the application the JVM is in an undefined state
          throw ex
      }.get

  def run[A, E](request: HttpRequest)
               (implicit successFormat: JsonFormat[A],
                errorFormat: JsonFormat[E]): Result[Task, A] =
    for {
      _ <- logger.info(s"start requesting ${request.uri.toString()}")
      response <- Task.deferFuture(http.singleRequest(request))
      _ <- logger.info(s"start collecting response entity")
      content <-
        Concurrency.withMaterializer[Task[String]] { implicit mat =>
          Task.deferFuture(
            response.entity.dataBytes.runFold(new StringBuilder) {
              case (acc, item) =>
                acc.append(item.utf8String)
            }.map(_.toString())
          )
        }
      _ <- logger.info("start parsing response content")
      data = if (response.status.isSuccess()) {
        parseJson(content, successFormat)
      } else {
        parseJson(content, errorFormat) match {
          case Right(apiError) =>
            Left(
                ServiceError(
                  response.status.intValue(),
                  Some(apiError)
                ): AppError
              )
          case Left(err) =>
            Left(
                AppError.combine(
                  err,
                  ServiceError(response.status.intValue(), Some(content))
                )
              )
        }
      }
      _ <- logger.info("request successfully processed")
    } yield data
}
