package com.github.chaabaj.githubstats.core

import monix.eval.Task

sealed trait LogEffect
case object LogInfo extends LogEffect
case object LogWarn extends LogEffect
case object LogError extends LogEffect


class TaskLogger(name: String) extends Logger[Task] {
  private val logger = java.util.logging.Logger.getLogger(name)

  def info(msg: String): Task[LogEffect] =
    Task {
      logger.info(msg)
      LogInfo
    }

  def warn(msg: String): Task[LogEffect] =
    Task {
      logger.warning(msg)
      LogWarn
    }

  def error(msg: String): Task[LogEffect] =
    Task {
      logger.severe(msg)
      LogError
    }
}
