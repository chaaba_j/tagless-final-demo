package com.github.chaabaj.githubstats.core

import scala.annotation.{switch, tailrec}

object test {

  @switch
  def test(n: Int): Boolean = {
    n match {
      case 2 => true
      case 3 => true
      case 4 => true
      case _ => false
    }
  }
}

object TraverseEither {

  def sequence[E, B](items: Seq[Either[E, B]]): Either[E, Seq[B]] = {
    @tailrec
    def sequenceTailRec(acc: Either[E, Seq[B]], rest: Seq[Either[E, B]]): Either[E, Seq[B]] =
      rest match {
        case Right(value) :: xs =>
          sequenceTailRec(acc.map(_ :+ value), xs)
        case Left(err) :: _ =>
          Left(err)
        case Nil =>
          acc
      }
    sequenceTailRec(Right(Seq.empty[B]): Either[E, Seq[B]], items)
  }
}
