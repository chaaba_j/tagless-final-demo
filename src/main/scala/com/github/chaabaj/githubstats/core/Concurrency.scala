package com.github.chaabaj.githubstats.core

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}
import monix.execution.Scheduler

import scala.concurrent.ExecutionContext

trait Dispatcher[T] {
  def scheduler: Scheduler
}
trait IOD
trait GlobalD

object Concurrency {
  private val actorSystem: ActorSystem = ActorSystem()
  private val materializer: Materializer = ActorMaterializer()(actorSystem)

  def withActorSystem[T](f: ActorSystem => T): T =
    f(actorSystem)

  def withMaterializer[T](f: Materializer => T): T =
    f(materializer)

  object dispatchers {
    val global: Dispatcher[GlobalD] =
      new Dispatcher[GlobalD] {
        val scheduler: Scheduler =
          Scheduler(actorSystem.dispatchers.defaultGlobalDispatcher)
      }
  }
}
