package com.github.chaabaj.githubstats.core

trait Logger[F[_]] {
  def info(msg: String): F[LogEffect]
  def warn(msg: String): F[LogEffect]
  def error(msg: String): F[LogEffect]
}
