package com.github.chaabaj.githubstats.repository

import com.github.chaabaj.githubstats.core.formatters.BaseJsonFormatters.IdFormat
import com.github.chaabaj.githubstats.repository.datas.{Repository, RepositoryResult, User}
import spray.json.DefaultJsonProtocol._

object JsonFormatters {

  implicit val idRepo = new IdFormat[Repository]
  implicit val idUserFormat = new IdFormat[User]
  implicit val userFormat = jsonFormat2(User)
  implicit val repoFormat = jsonFormat11(Repository)
  implicit val repoResultFormat = jsonFormat3(RepositoryResult)
}
