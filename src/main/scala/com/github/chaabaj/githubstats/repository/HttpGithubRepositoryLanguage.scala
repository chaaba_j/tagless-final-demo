package com.github.chaabaj.githubstats.repository

import akka.http.scaladsl.model.{HttpMethods, HttpRequest, Uri}
import com.github.chaabaj.githubstats.core.Result.{AppError, HandleError, Result}
import com.github.chaabaj.githubstats.core.{TaskLogger, TraverseEither}
import com.github.chaabaj.githubstats.repository.datas.RepositoryT.Languages
import com.github.chaabaj.githubstats.core.datas.{ApiError, Credentials}
import com.github.chaabaj.githubstats.core.http.Request
import com.github.chaabaj.githubstats.repository.datas.Repository
import monix.eval.Task
import monix.reactive.Observable

class HttpGithubRepositoryLanguage(credentials: Credentials)
  extends GithubRepositoryLanguage[Task] {

  import com.github.chaabaj.githubstats.core.formatters.BaseJsonFormatters._
  import spray.json.DefaultJsonProtocol._

  private val logger = new TaskLogger("http-repository-language-fetcher")

  override def resolve(repository: Repository): Result[Task, Languages] = {
    val uri = Uri(repository.languages_url)
    val request = HttpRequest(
      method = HttpMethods.GET,
      uri = uri
    )
    for {
      _ <- logger.info(s"request repositories ${repository.languages_url}")
      response <- Request.run[Languages, ApiError](request)
    } yield response
  }

  override def resolveAll(repositories: Seq[Repository]): Result[Task, Seq[Languages]] =
    Observable.fromIterator(repositories.iterator)
      .mapParallelUnordered(4)(resolve)
      .foldLeftL(Seq.empty[Either[AppError, Languages]])(
        (responses, response) => responses :+ response
      )
      .map(TraverseEither.sequence)
}
