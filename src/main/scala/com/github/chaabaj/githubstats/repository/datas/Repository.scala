package com.github.chaabaj.githubstats.repository.datas

import com.github.chaabaj.githubstats.core.datas.Id

case class User(
  id: Id[User],
  login: String
)

case class Repository(
  id: Id[Repository],
  name: String,
  owner: User,
  full_name: Option[String],
  stargazers_count: Int,
  watchers_count: Int,
  language: Option[String],
  languages_url: String,
  open_issues_count: Int,
  forks_count: Int,
  score: Int
)

case class Empty()

object RepositoryT {
  type Languages = Map[String, Int]
}