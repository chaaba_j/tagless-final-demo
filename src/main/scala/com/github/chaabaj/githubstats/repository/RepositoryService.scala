package com.github.chaabaj.githubstats.repository

import cats.Monad
import com.github.chaabaj.githubstats.core.Result.HandleError
import com.github.chaabaj.githubstats.core.Result.Result
import com.github.chaabaj.githubstats.repository.datas.Repository

class RepositoryService[F[_]: Monad](githubRepository: GithubRepository[F],
                                     githubRepoLang: GithubRepositoryLanguage[F]) {

  def search(keyword: String): Result[F, Seq[Repository]] = {
    val res = for {
      repos <- HandleError(githubRepository.list(keyword, 0, limit = 5))
      _ <- HandleError(githubRepoLang.resolveAll(repos))
    } yield repos
    res.value
  }
}
