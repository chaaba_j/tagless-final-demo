package com.github.chaabaj.githubstats.repository

import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model.{HttpMethods, HttpRequest, Uri}
import com.github.chaabaj.githubstats.core.Result.{HandleError, Result}
import com.github.chaabaj.githubstats.core.TaskLogger
import com.github.chaabaj.githubstats.core.datas.{ApiError, Credentials}
import com.github.chaabaj.githubstats.core.http.Request
import com.github.chaabaj.githubstats.repository.datas.{Repository, RepositoryResult}
import monix.eval.Task

class HttpGithubRepository(credentials: Credentials) extends GithubRepository[Task] {
  import com.github.chaabaj.githubstats.core.formatters.BaseJsonFormatters._
  import JsonFormatters._

  private val logger: TaskLogger = new TaskLogger("http-repository-fetcher")

  override def list(keyword: String, offset: Int, limit: Int): Result[Task, Seq[Repository]] = {
    val params = Map(
      "q" -> s"$keyword in:name",
      "type" -> "repositories",
      "page" -> offset.toString,
      "per_page" -> limit.toString,
      "sort" -> "stars",
      "order" -> "desc",
      "access_token" -> credentials.token
    )
    val request = HttpRequest(
      method = HttpMethods.GET,
      uri = Uri(s"${credentials.url}/search/repositories")
        .withQuery(Query(params))
    )
    HandleError(
      for {
        _ <- logger.info(s"request repositories $offset $limit")
        _ <- logger.info(request.uri.toString())
        response <- Request.run[RepositoryResult, ApiError](request)
      } yield response
    ).map(_.items).value
  }
}
