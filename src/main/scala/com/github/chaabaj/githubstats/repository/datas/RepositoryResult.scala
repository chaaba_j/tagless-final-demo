package com.github.chaabaj.githubstats.repository.datas

case class RepositoryResult(
  total_count: Int,
  incomplete_results: Boolean,
  items: Seq[Repository]
)
