package com.github.chaabaj.githubstats.repository

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.{Directives, Route}
import com.github.chaabaj.githubstats.core.Concurrency
import monix.eval.Task
import spray.json.DefaultJsonProtocol._
import com.github.chaabaj.githubstats.repository.JsonFormatters._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

import scala.util.{Failure, Success}

class Routes(searchModule: RepositoryModule[Task]) extends Directives {

  private val scheduler =
    Concurrency.dispatchers.global.scheduler

  private val searchTask = searchModule.searchService.search("scala")

  private val searchRepositories: Route =
    path("search" / "repositories") {
      get {
        onComplete(searchTask.runAsync(scheduler)) {
          case Success(Right(repositories)) =>
            complete(repositories)
          case Success(Left(error)) =>
            error.printStackTrace()
            complete(StatusCodes.BadRequest)
          case Failure(ex) =>
            ex.printStackTrace()
            complete(StatusCodes.ServiceUnavailable)
        }
      }
    }

  val routes: Route =
    searchRepositories
}
