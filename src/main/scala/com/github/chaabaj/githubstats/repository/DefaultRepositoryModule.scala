package com.github.chaabaj.githubstats.repository

import com.github.chaabaj.githubstats.Infrastructure
import monix.eval.Task

class DefaultRepositoryModule(infra: Infrastructure) extends RepositoryModule[Task] {
  lazy val searchService: RepositoryService[Task] =
    new RepositoryService[Task](
      new HttpGithubRepository(infra.githubCredential),
      new HttpGithubRepositoryLanguage(infra.githubCredential)
    )
}
