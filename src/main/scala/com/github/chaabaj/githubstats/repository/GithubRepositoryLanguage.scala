package com.github.chaabaj.githubstats.repository

import com.github.chaabaj.githubstats.core.Result.Result
import com.github.chaabaj.githubstats.repository.datas.RepositoryT.Languages
import com.github.chaabaj.githubstats.core.datas._
import com.github.chaabaj.githubstats.repository.datas.Repository

trait GithubRepositoryLanguage[F[_]] {
  def resolve(repository: Repository): Result[F, Languages]
  def resolveAll(repositories: Seq[Repository]): Result[F, Seq[Languages]]
}