package com.github.chaabaj.githubstats.repository

import com.github.chaabaj.githubstats.core.Result.Result
import com.github.chaabaj.githubstats.repository.datas.Repository

trait GithubRepository[F[_]] {
  def list(keyword: String, offset: Int, limit: Int = 100): Result[F, Seq[Repository]]
}

