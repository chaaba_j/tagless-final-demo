package com.github.chaabaj.githubstats.repository

trait RepositoryModule[F[_]] {
  def searchService: RepositoryService[F]
}

