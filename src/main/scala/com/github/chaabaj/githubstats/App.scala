package com.github.chaabaj.githubstats

import com.typesafe.config.ConfigFactory

import scala.io.StdIn

object App {
  def main(args: Array[String]): Unit = {
    val infra = new Infrastructure(ConfigFactory.load())
    Server.start(3000, infra)
    StdIn.readLine()
  }
}
