package com.github.chaabaj.githubstats

import akka.http.scaladsl.server.{Directives, Route}
import com.github.chaabaj.githubstats.core.Concurrency
import com.github.chaabaj.githubstats.core.datas.Credentials
import com.github.chaabaj.githubstats.repository._
import com.github.chaabaj.githubstats.user.DefaultUserModule

class Routes(infra: Infrastructure) extends Directives {

  private implicit val exc = Concurrency.dispatchers.global.scheduler

  private val repositoryModule = new DefaultRepositoryModule(infra)
  private val userModule = new DefaultUserModule(infra)

  val routes: Route =
    new repository.Routes(repositoryModule).routes ~
    new user.Routes(userModule).routes
}
