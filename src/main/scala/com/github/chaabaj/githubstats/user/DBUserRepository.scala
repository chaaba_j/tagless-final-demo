package com.github.chaabaj.githubstats.user

import com.github.chaabaj.githubstats.core.Result.{NotFound, Result}
import com.github.chaabaj.githubstats.core.datas.{Hashed, Id}
import com.github.chaabaj.githubstats.user.datas.User
import monix.eval.Task
import slick.lifted.{ProvenShape, Tag}
import slick.jdbc.MySQLProfile.api._

private class UserTable(tag: Tag) extends Table[User](tag, "users") {

  def id: Rep[Int] = column[Int]("id")
  def name: Rep[String] = column[String]("name")
  def hashedPassword: Rep[String] = column[String]("hash_password")

  private val toUser = (id: Int, name: String, hashedPassword: String) =>
    User(
      id = Id[User](id),
      name = name,
      hashedPassword = Some(Hashed(hashedPassword))
    )

  private val toRecord = (user: User) =>
    Some(
      (
        user.id.value,
        user.name,
        ""
      )
    )

  override def * : ProvenShape[User] =
    (id, name, hashedPassword) <> (toUser.tupled, toRecord)
}

class DBUserRepository(db: Database) extends UserRepository[Task] {
  private val userTable = TableQuery[UserTable]

  override def byNameAndPassword(name: String, hashPassword: Hashed): Result[Task, User] =
    Task.deferFutureAction { implicit scheduler =>
      db.run(
        userTable
          .filter(c => c.name === name && c.hashedPassword === hashPassword.value)
          .result
          .headOption
      )
    }.map {
      case Some(user) => Right(user)
      case None => Left(NotFound[User]("name or password doesn't match"))
    }
}
