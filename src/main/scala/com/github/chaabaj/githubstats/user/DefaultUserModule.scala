package com.github.chaabaj.githubstats.user

import com.github.chaabaj.githubstats.Infrastructure
import monix.eval.Task

class DefaultUserModule(infra: Infrastructure) extends UserModule[Task] {
  override val authService: AuthService[Task] =
    new TaskAuthService(
      new DBUserRepository(infra.db)
    )
}
