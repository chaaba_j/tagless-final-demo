package com.github.chaabaj.githubstats.user


trait UserModule[F[_]] {
  def authService: AuthService[F]
}