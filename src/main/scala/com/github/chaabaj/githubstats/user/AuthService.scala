package com.github.chaabaj.githubstats.user

import cats.Monad
import com.github.chaabaj.githubstats.core.{Concurrency, Hash}
import com.github.chaabaj.githubstats.core.Result.{HandleError, Result}
import com.github.chaabaj.githubstats.core.datas.Hashed
import com.github.chaabaj.githubstats.user.datas.User
import monix.eval.Task

trait AuthService[F[_]] {

  def userRepository: UserRepository[F]

  protected implicit def monadF: Monad[F]

  protected def hash(password: String): Result[F, Hashed]

  def auth(name: String, password: String): Result[F, User] = {
    (for {
      hashedPassword <- HandleError(hash(password))
      user <- HandleError(userRepository.byNameAndPassword(name, hashedPassword))
    } yield user).value
  }
}

class TaskAuthService(override val userRepository: UserRepository[Task]) extends AuthService[Task] {
  override protected implicit def monadF: Monad[Task] = implicitly[Monad[Task]]

  override protected def hash(password: String): Result[Task, Hashed] =
    Task(Hash.bcrypt(password))
      .executeOn(Concurrency.dispatchers.global.scheduler)
}
