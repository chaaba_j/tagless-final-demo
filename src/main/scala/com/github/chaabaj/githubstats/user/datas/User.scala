package com.github.chaabaj.githubstats.user.datas

import com.github.chaabaj.githubstats.core.datas.{Hashed, Id}

case class User(id: Id[User],
                name: String,
                hashedPassword: Option[Hashed] = None)
