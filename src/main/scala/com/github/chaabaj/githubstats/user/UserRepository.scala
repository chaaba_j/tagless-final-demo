package com.github.chaabaj.githubstats.user

import com.github.chaabaj.githubstats.core.Result.Result
import com.github.chaabaj.githubstats.core.datas.{Hashed, Id}
import com.github.chaabaj.githubstats.user.datas.User

trait UserRepository[F[_]] {
  def byNameAndPassword(name: String, hashPassword: Hashed): Result[F, User]
}
