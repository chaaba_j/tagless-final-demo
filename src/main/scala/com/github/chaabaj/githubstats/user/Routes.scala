package com.github.chaabaj.githubstats.user

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.{Directives, Route}
import com.github.chaabaj.githubstats.core.Concurrency
import monix.eval.Task
import spray.json.DefaultJsonProtocol._
import com.github.chaabaj.githubstats.user.JsonFormatters._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import scala.util.{Failure, Success}

class Routes(userModule: UserModule[Task]) extends Directives {

  private val scheduler =
    Concurrency.dispatchers.global.scheduler

  private val auth: Route =
    path("search" / "repositories") {
      get {
        val authTask = userModule.authService.auth("toto", "blabla")
        onComplete(authTask.runAsync(scheduler)) {
          case Success(Right(user)) =>
            complete(StatusCodes.OK)
          case Success(Left(error)) =>
            error.printStackTrace()
            complete(StatusCodes.BadRequest)
          case Failure(ex) =>
            ex.printStackTrace()
            complete(StatusCodes.ServiceUnavailable)
        }
      }
    }

  val routes: Route =
    auth
}
