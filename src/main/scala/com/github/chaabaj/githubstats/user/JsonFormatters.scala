package com.github.chaabaj.githubstats.user

import com.github.chaabaj.githubstats.core.datas.Id
import com.github.chaabaj.githubstats.core.formatters.BaseJsonFormatters.IdFormat
import com.github.chaabaj.githubstats.user.datas.User
import spray.json.{DefaultJsonProtocol, DeserializationException, JsNumber, JsObject, JsString, JsValue, JsonFormat}

object JsonFormatters extends DefaultJsonProtocol {

  implicit val idUser = new IdFormat[User]
  implicit val userFormat = new JsonFormat[User] {
    override def read(json: JsValue): User =
      json.asJsObject.getFields("id", "name") match {
        case Seq(JsNumber(id), JsString(name)) =>
          User(Id[User](id.toInt), name, None)
        case _ =>
          throw DeserializationException(s"invalid json ${json.prettyPrint}")
      }

    override def write(user: User): JsValue =
      JsObject(
        "id" -> JsNumber(user.id.value),
        "name" -> JsString(user.name)
      )
  }
}
