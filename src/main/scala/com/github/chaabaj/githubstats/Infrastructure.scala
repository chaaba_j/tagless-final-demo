package com.github.chaabaj.githubstats

import com.github.chaabaj.githubstats.core.datas.Credentials
import com.typesafe.config.Config
import slick.jdbc.MySQLProfile.api._

class Infrastructure(config: Config) {
  val db: Database = Database.forConfig("db", config)
  val githubCredential: Credentials =
    Credentials(
      config.getString("github.url"),
      config.getString("github.token")
    )
}
